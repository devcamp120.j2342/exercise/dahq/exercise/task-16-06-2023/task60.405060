package com.devcamp.projectcarapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "typecars")
public class TypeCar {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private int type_id;
    @Column(name = "type_code", unique = true)
    private String typeCode;
    @Column(name = "type_name")
    private String typeName;
    @ManyToOne
    @JoinColumn(name = "car_id")
    private Car cars;

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public TypeCar(String typeCode, String typeName) {
        this.typeCode = typeCode;
        this.typeName = typeName;
    }

    public TypeCar() {
    }

}
