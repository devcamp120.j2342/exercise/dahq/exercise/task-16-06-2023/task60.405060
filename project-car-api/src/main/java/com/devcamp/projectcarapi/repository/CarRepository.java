package com.devcamp.projectcarapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.projectcarapi.model.Car;

public interface CarRepository extends JpaRepository<Car, Integer> {
    Car findByCarCode(String carCode);

}
