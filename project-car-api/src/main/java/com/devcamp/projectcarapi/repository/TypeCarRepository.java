package com.devcamp.projectcarapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.projectcarapi.model.TypeCar;

public interface TypeCarRepository extends JpaRepository<TypeCar, Integer> {

}
