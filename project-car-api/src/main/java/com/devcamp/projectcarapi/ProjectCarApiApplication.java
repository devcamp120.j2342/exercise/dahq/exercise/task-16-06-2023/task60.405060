package com.devcamp.projectcarapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectCarApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectCarApiApplication.class, args);
	}

}
