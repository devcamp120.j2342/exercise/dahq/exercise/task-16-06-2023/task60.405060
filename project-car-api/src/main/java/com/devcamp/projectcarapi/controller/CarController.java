package com.devcamp.projectcarapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.projectcarapi.model.Car;
import com.devcamp.projectcarapi.model.TypeCar;
import com.devcamp.projectcarapi.repository.CarRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CarController {
    @Autowired
    CarRepository carRepository;

    @GetMapping("/devcamp-car")
    public ResponseEntity<List<Car>> getAllCars() {
        try {
            List<Car> cars = new ArrayList<Car>();
            carRepository.findAll().forEach(cars::add);
            return new ResponseEntity<>(cars, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.OK);
        }

    }

    @GetMapping("/devcamp-cartype")
    public ResponseEntity<Set<TypeCar>> getTypeCar(@RequestParam(required = true, name = "carCode") String carCode) {
        try {
            Car cars = carRepository.findByCarCode(carCode);
            if (cars != null) {
                return new ResponseEntity<>(cars.getTypes(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
